\section{Introduction}
\label{sec:Introduction}

Home security systems are essential to a family for the protection of properties and the maintenance of household safety from potential break-ins. 
Different technologies and solutions for mining biometric information to implement unsolicited intrusion detection have been proposed ~\cite{nixon2006automatic, orr2000smart, gafurov2007survey, wang2016gait, zeng2016wiwho, zhang2016wifi}, mainly relying on surveillance cameras, floor sensors, wearable sensors, and Wi-Fi devices.
By extracting desirable features from captured data (e.g., video from cameras, foot pressure from floor sensors, stride and velocity reported by wearable sensors, and channel state information (CSI) from Wi-Fi devices), the unique patterns for an individual can be spotted. 
Some solutions have been commercialized in the professional home security  systems to conduct the effective intrusion detection tasks. 
However, the Parks Associates report~\cite{household_security} shows that only $27\%$ of the U.S. households have adopted home security systems,  for which the most commonly cited reason is the high cost of such professional systems incurred by the purchase of expensive devices or service fees. 
Undoubtedly, many people do not envision the security as a necessary expense and are still reluctant to invest in home security. 
Surveillance camera is the most popular home security solution, in addition to extra cost for purchasing the devices,  many serious privacy concerns have been raised by the community on the private and sensitive video data, whereas a remote attacker may have the chance to gain access to the video captured by the camera \cite{valente2019privacy} or infer the action of the victim from the encrypted video stream \cite{li2016side,li2020your}.
The newly appeared Wi-Fi CSI-based human identification solution is a viable method that probably can cut down the deployment cost to some extent.
However, the uneven distribution of the CSI Fresnel zone and the noise of  electromagnetic signals itself make the obtained Wi-Fi signals unstable \cite{wu2017device}.
In addition, Wi-Fi CSI-based solutions keep sending CSI measurement packets, which  inevitably occupy Wi-Fi channel resources and impact the performance of nearby Wi-Fi devices \cite{ma2019wifi}.
In comparison, a self-installing software-based system at a negligible expense is highly desired, especially given the proliferation of smart home devices.
 



The functional components of  appliances in smart homes offer new opportunities to explore their sensing capabilities for home security, especially in authentication with biometrics.
As a promising type of such components, the audio system equipped with internal speaker and microphone can be well leveraged to implement new smart home sensing. 
As an example, voice assistant systems, equipped with speakers and microphones, have played important roles in smart homes by detecting voice commands and making timely responses.
According to a report~\cite{Rishi_2018_google} from Google, voice assistant systems are available on more than 400 million devices, including Google Home, Amazon Echo, Apple HomePod, Aristotle, DingDong, Rokid, Mi AI Speaker, smartphones,  headphones, TVs, smartwatches, and others. 
The popularity of  voice systems promotes the recent research interest in exploring the  potential sensing capabilities of built-in speakers and microphones, which are ``always-on'' in these systems, to serve new needs in smart homes.
A critical direction of such an exploration, which has been pervasively attracting research community's  interest, is to enable the sensing capability of voice assistant devices using acoustic signals.


Many research efforts have been made to explore the sensing abilities of acoustic signals on objects, shapes, and environmental dynamics.
For example, in~\cite{waite2002sonar}, Sonar has been developed to gather information about distant objects such as range, angle, and velocity using sound propagation. 
In Internet-of-Things (IoT) applications, the inaudible acoustic signal at 20kHz has been utilized to sense human activities.
Specifically, in~\cite{gupta2012soundwave,nandakumar2016fingerio}, the acoustic signal is used to estimate human motion and gesture, while in~\cite{sun_2018_vskin}, the surface of mobile devices has been sensed to enable touchscreen functions. 
Moreover, acoustic signals have also been used to capture mouth movements in order to combat voice replay attacks~\cite{lu2018lippass, zhang2017hearing}.
However, all existing solutions require the device to be close to the body part, 
making them unsuitable in smart homes for intrusion detection.
Despite some research efforts~\cite{nandakumar2017covertband, graham15:IoT:software} extending the acoustic sensing capabilities to cover a larger range of distance, they only apply to coarse-grained detection while relying on the known environment (i.e., floor plan), failing to meet the requirements of smart home environments.



To advance the acoustic sensing in IoT applications, we propose EchoSensor, a human authentication system which uses the built-in speaker and microphone on the ubiquitous audio, voice, or other devices in smart homes to capture  human biometric information (i.e., gait) for the purpose of intrusion detection.
Specifically, EchoSensor controls the speaker to transmit inaudible acoustic signals around 20kHz and leverages the microphone to capture the reflected signals. 
When the acoustic signal is bounced off from a walking human, variations in the frequency domains will be observed in the reflected signals, due to the Doppler effect, which may enclose the rich gait patterns representing the unique   information of this person.
Although there  exist some solutions to explore the gait-based human identification using acoustic signals, they leverage the professional devices \cite{zhang2007acoustic, wang2018gait, le2018human}, i.e., Doppler radar or acoustic sensor, which have the powerful acoustic signal transmitting function and strong signal resolution capability to ensure the capture of high-quality Doppler effect spectrogram, without mining the fine-grained gait features.
Such solutions cannot be applied to our proposed EchoSensor system, as we only rely on the readily available audio or voice devices at smart home, rather than purchasing extra professional devices to implement the individual recognition for the intrusion detection purpose.


 
The goal of this paper is to explore the  biometric information of a walking human through inaudible acoustic sensing, which is used as the basis for the design and implementation of EchoSensor.
To achieve this goal, we generate the spectrogram of reflected signals with Doppler shift and develop signal processing techniques to mitigate interference/noise.
With the pure acoustic signals acquired as a result, clear gait patterns can be mined by EchoSensor for the use of individual recognition. 
In particular, we first develop solution to let EchoSensor be capable of detecting the starting point of a walking man and identifying his gait cycle time to differentiate between the  independent gaits. 
With walking steps separated into a set of two-gait samples, the unique and fine-grained features of an individual, i.e., cepstral coefficient vector, leg contour curve, spectrum signature, and torso speed and gait cycle time pairs, are  extracted via a series of signal processing techniques. 
To realize the eventual goal of intrusion detection, all family members of a household will enroll in EchoSensor, with their gait features extracted and trained through the machine learning classifier, resulting in a detector for them.
Once an individual is sensed, his gait patterns with fine-grained features are  mined by EchoSensor. 
Then, the detector can automatically classify him as a family member or an intruder.

Compared to other home security systems based on surveillance cameras \cite{nixon2006automatic}, floor sensors \cite{orr2000smart}, wearable sensors \cite{gafurov2007survey} or Wi-Fi signals \cite{wang2016gait, zeng2016wiwho, zhang2016wifi}, EchoSensor possesses a set of salient features, including but are not limited to: 
{\em First}, it is a software-based system that can be self-installed in existing home audio or voice devices, which is hard to be notified and compromised by an unsolicited intruder. 
{\em Second}, it leverages the speakers and microphones equipped in existing home appliances, without incurring extra cost. 
The fine-grained features extracted by EchoSensor are sufficient to distinguish an individual.
{\em Third}, it is based on acoustic signals rather than radio frequency signals, thus relieving the competition for radio spectrum resource  in smart homes.




We implement EchoSensor on Huawei p20 with Android 8.1 and a speaker of Bose Soundlink Revolve.
Beyond our proof-of-concept implementation, EchoSensor is also envisioned to work effectively on many other smart home audio and voice devices, such as Google Home, Amazon Echo, Apple HomePod, etc.,  which are suitable for the home scenario of intrusion detection because they  locate at different places, covering the majority of regions in a house.
We have evaluated the performance of EchoSensor in 10 households involving a total of 24 participants in terms of intruders and family members detection. 
Experimental results show that EchoSensor can achieve the averaged Intruder Gait Detection Rate (IDR) and True Family Member Gait Detection Rate (TFR) of $92.7\%$ and $91.9\%$, respectively.  
The intrusion alarm accuracy can reach $98\%$ and $100\%$, respectively, with only 5 and 10 detected two-gait samples.


Our contributions can be summarized as follows. 
\vspace{-0.3em}
\begin{itemize}
	\item To the best of our knowledge, EchoSensor is the first work to demonstrate the feasibility of moderate-range (5 meters) gait-based user authentication using speakers and microphones on pervasively existed audio or voice devices in smart homes. 
	
	\item The new solutions based on the signal processing techniques are developed in EchoSensor to perform the fine-grained analysis so as to acquire the unique human gait patterns.
	In particular, EchoSensor can detect the starting point of a walking human and identify his gait cycle time to differentiate independent gaits. 

	
	\item We develop new techniques to identify and extract a series of fine-grained features from low quality Doppler signals to represent a person's walking patterns. With these features, a machine learning classifier is trained and individual recognition is implemented automatically.


	\item Extensive experiments are  conducted to verify the performance of EchoSensor. 
	Experimental results demonstrate that EchoSensor can achieve high accuracy of intrusion detection in smart homes.
\end{itemize}

 