\section{SYSTEM DESIGN}
\label{sec:sys_des}

In this section, we propose our design of EchoSensor, which leverages the built-in speaker and microphone in smart home audio devices to enable acoustic sensing capabilities. With the speaker emitting inaudible ultrasonic signals and the microphone receiving reflected signals, fine-grained analysis of reflected signals is employed to extract the enclosed biometric information for individual recognition and intrusion detection.
In our design of EchoSensor, a number of technical challenges were encountered to be addressed, summarized as follows.


	{\em First, } the reflected acoustic signals are typically more coarse and of lower quality with much underlying interference and noise, compared to those from professional devices.
	As most acoustic signals suffer from fast attenuation, the interference and noise are grossly detrimental to Doppler shift measurement and may totally destroy the embedded biometric patterns.
	It is necessary yet challenging to develop effective interference cancellation or mitigation solutions toward the acquisition of pure acoustic signals so as to mine the accurate biometric information.
	

	{\em Second, } as EchoSensor aims to only rely on the reflected signals for human recognition without the help of additional equipment for signal resolution, it is significant to determine the useful information that can characterize human walking patterns.
	Thus, we need to develop solutions to automatically identify the starting point of a human walking and gait-cycle time from the Doppler effect signals, which are challenging, especially when the Doppler effects are buried in strong interference and noise.

	{\em Third,} to recognize an individual, the effective fine-grained features that can represent an individual's behaviors shall be identified, extracted, and well utilized.
	However, EchoSensor can only rely on the Doppler signals to acquire such a set of  features, where a series of signal processing techniques have to be developed to conduct the fine-grained analysis.





\subsection{Threat Model}


The goal of an attacker is to intrude into the victim's home without being detected by EchoSensor.
We assume a strong intruder  has the ability to transmit ultrasonic signal, but could not directly access the EchoSensor system.

\smallskip

\noindent{\bf No device access.} We assume an attacker cannot immediately control or turn off the audio or voice devices after entering the house. 
However, he may be aware of the existence of EchoSensor and knows its principle that  Doppler gait signal will be captured for intrusion detection.
He could choose any specific action to reduce the possibility of being detected, such as mimicking the victim's gait patterns.

\smallskip
\noindent{\bf No owner interaction.} We assume an intruder could not ask the victim to turn off the system.
The EchoSensor is ``always-on'' when the victims leave their house. 
Also, the intruder is assumed to be present during the victims' absence. 
That means the victims will not meet the intruder.

\smallskip
\noindent{\bf Replay and saturate attack.} We assume the intruder has the knowledge for signal processing. He could process the ultrasonic signal and has the ability to play ultrasonic signal to EchoSenor to obfuscate the system.
For example, he may be the victims' friend and record their gait signals to conduct replay attack when the victims are not at home. 
The intruder may also play the high-energy signal in front of EchoSensor to conduct the saturate attack.


\vspace{-0.2em}
\subsection{System Overview}


\begin{figure*}
	\centering
	\includegraphics[width=5.5in]{Fig/systemdesign.pdf}
	\caption{The workflow of EchoSensor.}
	\label{systemdesign}
\end{figure*}

Figure~\ref{systemdesign} exhibits the system design of EchoSensor, consisting of five components: {\em Sensing,  Signal Processing, Gait Pattern Mining, Feature Extraction,} and {\em Recognition}.
In the {\em Sensing module}, EchoSensor programs the built-in speaker in audio devices to emit inaudible acoustic signals at 20kHz, and uses the microphone to collect Doppler effect signals with the sampling frequency of 44.1kHz.
Such a sampling frequency ensures it is twice higher than the highest Doppler shift frequency, so the Doppler signal can be entirely reconstructed from the recorded signal~\cite{shannon1998communication}.

The received signals  pass to the {\em Signal Processing module} through a high-pass Butterworth filter \cite{mello2007digital}. 
The interference and noise will be canceled or mitigated here so that the expected pure acoustic signals can be acquired.
To mine the gait patterns, EchoSensor has two sub-modules in the {\em Gait Pattern Mining module}, i.e., walking detection and gait cycle time identification, where the former one is to determine the starting point of a human walking while the latter determines the gait time so that each independent gait can be differentiated.
After this stage, the human walking steps can be separated into a set of two-gait samples, which will be sent to the {\em Feature Extraction module}.
In this module, we focus on four  categories  of features for extraction, i.e.,  cepstral coefficient vector of each two-gait sample, spectrum signature, ratio curve between the leg speed curve and torso speed curve, and gait cycle time. 
In the end, we employ a machine learning classifier to recognize an individual in the {\em Recognition module}.
We next illustrate the detailed design of each module.






\begin{figure}
	\centering
	\includegraphics[width=2.5in]{Fig/original2.pdf}
	\caption{Spectrogram of  Doppler effect from the gait movement.}
	\label{original spectrogram}
\end{figure}

\subsection{Interference Cancellation}
\label{subsec:IC}


After receiving the reflected acoustic signals at the microphone, we generate the spectrogram of the Doppler shift for analysis.
Figure~\ref{original spectrogram} is an example of the spectrogram from reflected signals, where the short-time Fourier transform (STFT) is employed to process them.
During the STFT process, the original signal is sliced into a set of small bins, with each bin having a 0.3 seconds duration.
The overlapping between each two consecutive bins is set to $95\%$. 
We then multiply each bin with the Hamming window and apply the  8192 point Fast Fourier transformation (FFT) on each bin to compose the spectrogram.	
As the human walking speed is estimated up to a maximum of 1.5m/s \cite{transafety1997study}, and the maximum leg speed is up to 5m/s, the reflected signal will have a maximum shift of $500$Hz, according to Eqn.~(\ref{velocity}).
Thus, in Figure~\ref{original spectrogram}, we only need to consider the spectrogram between 19.5kHz and 20.5kHz,  sufficiently covering all the Doppler shifts bounced off from a moving body.
We also observe the blurred shape of the Doppler effect between 20kHz and 20.5kHz, drowned in the noise and sub-harmonics.
Hence, the next critical step is to mitigate the interference and noise so as to locate the signal of interest.




	
Since the built-in speaker and microphone are omnidirectional, signals received by the microphone include not only desired reflection from the target, but also noise. 
Two major sources of noises are identified: direct transmission noise and background reflection noise, where the former one is the original audio played by the speaker while the late one is caused by the static reflector in the environment. 
Both of them overlap and interfere with the Doppler effect in  time domain which greatly distort the quality of Doppler effect spectrogram. 
To minimize the impact of interference, we leverage the fact that both direct transmission and background reflection do not generate Doppler shift as we assume 1) the speaker and microphone remain static, thus there is no relevant movement between them; 2) there is no moving object in the environment, so the environment is considered to be static. 
These assumptions guarantee that the aforementioned two types of noises do not change over time, allowing us to safely subtract them from the spectrogram of received signal.



As such, in our {\em Signal Processing Module}, we propose a two-stage scheme to eliminate all the existing interference and noise, in order to acquire a clear Doppler shift spectrogram.


\begin{itemize}
\item \textbf{Stage 1}: As discussed, the Doppler effect can only exist in the range of $20\pm 0.5$kHz, considering the human walking speed.
Hence, we pass the received signal to a high-pass Butterworth filter with  a cut-off frequency of 19kHz to obtain the spectrogram that is only located within 19.5kHz$\sim$20.5kHz.


\item \textbf{Stage 2}: This stage tries to remove the noise from direct transmission  and background.
Since these two types of noise stay static over time, their spectrograms would be static (i.e., no Doppler effect).
This allows us to eliminate interference by subtracting the spectrograms of noise from that of received signals. 
To acquire the background noise, we put the speaker in an empty room to generate the ultrasonic signal, where there is no moving object in front.
The microphone will record the background noises that are reflected from different directions, which will be stored in EchoSensor for the use of subtraction.
That is, EchoSensor eliminates the power from these static reflectors by simply subtracting the output of the FFT in background noise from the FFT of reflected signals.
This spectral subtraction method can eliminate all interference/noise from the static reflector in the environment.
Theoretically, the direct transmission noise can also be subtracted in this process.
However, the built-in speakers in  smart home devices are  not designed to generate such a high-frequency signal, which will cause the frequency of emitted ultrasonic wave to fluctuate slightly over time, thus cannot be eliminated by spectral subtraction.
Such a phenomenon has been verified in our extensive experiments, where we have consistently observed that the frequency generated by the speaker fluctuates from 19.96kHz to 20.04kHz.
Thus, we can set the received signal energy values between 19.95kHz and 20.05kHz to zeros for safely removing all the direct transmission noise without impacting the Doppler effect.
\end{itemize}


\begin{figure}
	\centering
	\includegraphics[width=2.5in]{Fig/denoised.pdf}
	\caption{The resulted spectrogram after applying two-stage cancellation.}
	\label{denoised Spectrogram}
\end{figure}

Figure~\ref{denoised Spectrogram} shows the resulted spectrogram after applying our two-stage scheme to Figure~\ref{original spectrogram}. 
We can see the clear waveform of Doppler effect after removing all interference/noise.
The waveform of the Doppler effect varies over time, representing the human is moving because it has different instantaneous speeds at different moments. 

\subsection{Walking Detection}
After getting the clear Doppler effect waveform, we need to determine the starting point of human walking so as to identify a person's gait.
This step is important as accurately detecting the walking activity can also ensure that EchoSensor is initiated only when a person is detected to be walking.
We now discuss how we can detect walking activity using acoustic data. 
Although the direct transmission and background noise are eliminated in Section~\ref{subsec:IC}, there may still exist some underlying interference.
We keep track of such noise level threshold as $N_t$, which is initialized by calculating the average energy level between 19.5kHz$\sim$20.5kHz at time $t_0$, the time after initiating the speaker for 0.5 seconds. 
The reason for choosing 19.5kHz$\sim$20.5kHz is that the Doppler effect only locates within this range.
$N_t$ is updated by Exponential Moving Average algorithm \cite{lawrance1977exponential} in silent time period, i.e.,  
\begin{equation}
N_t=(1-\alpha)N_{t-1}+\alpha E_t, \label{noise threshold}
\end{equation}
where $E_t$ is the current environment noise level (i.e., averaged energy level between 19.5kHz$\sim$20.5kHz) at time slice $t$, $N_{t-1}$ is the noise level threshold at time slice $t-1$, and $\alpha$ is set to 0.1 according to prior work on detecting the start of walking using Wi-Fi signals \cite{wang2016gait}. 
We have observed there is still some impulse noise in the spectrogram. 
To avoid the misunderstanding of the impulse noise as the beginning of the Doppler signal, we set the detecting threshold value as three times the noise level. 
Such a threshold value is set up based on our observation that it is higher than any impulse noise we have observed and is also beyond the energy level of the Doppler signal.
The Doppler signal caused by human motion is detected when the energy level between 19.5kHz$\sim$20.5kHz is above the detecting threshold value. 




\subsection{Gait Cycle Time Estimation}
\label{subsec:gaitcycletime}
We  next try to separate the spectrogram of Doppler effect into a set of two-gait samples.
The gait cycle time, defined as the time duration between two consecutive events that the right heel touches the ground, can be roughly estimated by visual inspection on the spectrogram of Figure~\ref{denoised Spectrogram}. 
We  elaborate our design of an estimation strategy that can automatically calculate the gait cycle time. 


Our estimation is based on the upper contour from the leg reflection.
As a person walks toward the speaker, the generated Doppler effect  will be within $f_{\min} \sim f_{\max}$, i.e., 20kHz$\sim$20.5kHz.
Likewise, when this person walks away from the speaker, $f_{\min}$ and $f_{\max}$ will be 19.5kHz and 20kHz, respectively.
To identify each point in the leg upper contour, we define a function $P(f, t)$ as follows:


\begin{equation}
P(f, t)=\frac{\sum_{f_{\min }}^{f} F(f, t)}{\sum_{f_{\min } }^{f_{\max }} F(f, t)}, \label{leg contour}
\end{equation}
where $F(f, t)$ represents the Doppler effect energy on the frequency $f$  at a certain time $t$ on the spectrogram, $\sum_{f_{\min } }^{f} F(f, t)$ represents the cumulative energy from $f_{\min}$ to a frequency $f$, and $\sum_{f_{\min } }^{f_{\max }} F(f, t)$ represents the total cumulative energy of the Doppler signal at time $t$.
Hence, $P(f, t)$ represents the ratio of cumulative energy that is lower than frequency $f$ over the total Doppler effect energy at time $t$. 
An appropriate value of $f$ at time $t$ in Doppler effect spectrogram needs to be obtained so as to identify a point in the upper contour. We set the threshold value of $P(f, t)$ as $95\%$ to derive the frequency $f$, following the suggestion in previous work \cite{van2008feature} which explored human movement model using Doppler radar signal.

\begin{figure}
	\centering
	\includegraphics[width=2.5in]{Fig/contour3.pdf}
	\caption{Upper contour of the marked spectrogram.}
	\label{pleg contour}	
\end{figure}


Human walking involves an acceleration phase and a uniform speed phase.
To get a steady walking, we should remove the acceleration phase while keeping the uniform speed phase as the sample. 
That is, we remove the first two-gait cycle of a walking sample, which represents the start of walking.
As a result, we obtain the upper contour in Figure~\ref{pleg contour} shown as the white curve, by applying the aforementioned method to the spectrogram in Figure~\ref{denoised Spectrogram}.
It is clearly observed that our method  tracks the variation of leg speed during each walking period.
We continue to process the leg contour curve by subtracting the frequency on the curve with the averaged frequency over all points. 
The resulted new curve is then passed to a low-pass filter with the cut-off frequency of 6Hz to get a smooth curve.
The valleys represent the lowest leg speed of a gait cycle, indicating a heel touches the ground, and the gait cycle time is the time duration between two consecutive touches on the ground of the right heel.




However, since FFT may cause frequency leakage, some noise power will render the valley offset on the spectrogram.
This may lead to some errors to the estimated gait cycle time, as the time corresponding to the valley does not necessarily represent the beginning of a gait cycle. 
To minimize the adverse impact of frequency leakage, we use the autocorrelation of the leg contour curve to robustly estimate gait cycle time, which is calculated by: 
\begin{equation}
R(\tau)=\sum_{t}(F(t)-\mu)(F(t-\tau)-\mu), \label{eq}
\end{equation}
where $\mu$ is the averaged frequency of the entire leg contour, $F(t)$ is the frequency of leg contour at time $t$, and $\tau$ represents the time shift. 
The autocorrelation is taken over a period of steady walking, which means that we take the autocorrelation on the smooth curve obtained after the processing of the low-pass filter.
Through autocorrelation, we can obtain a better estimation of the gait cycle time than directly searching for valleys on the leg contour curve. 
Each peak in the autocorrelation function indicates that the contour curve is similar to its original version when shifting for a time period of $\tau$. 
A gait cycle contains two footsteps where the leg speed changes twice, thus the leg contours are similar when shifting $L/2$, where $L$ is the gait cycle time. 
Figure~\ref{Autocorrelation} shows the contour curve after autocorrelation, where the first peak appears at the point $\tau = 0.62$ seconds.
Thus, the estimated gait cycle time $L = 2\tau$  is $1.24$ seconds. 
A gait cycle is also called as two-gait sample or instance in the following of this paper.
\begin{figure}
	\centering
	\includegraphics[width=2.5in]{Fig/autocorrelation0.pdf}
	\caption{Autocorrelation of the leg contour curve.}
	\label{Autocorrelation}
\end{figure}



\subsection{Feature Extraction}
\label{subsec:feature}


Fine-grained feature extraction is a critical step in EchoSensor to capture the unique biometric information of each individual. 
In the denoised spectrogram (Figure~\ref{denoised Spectrogram}), we extract four types of features related to the gait pattern.

The {\em first} feature is the cepstral coefficient vector \cite{oppenheim1968homomorphic}, which takes in account of the normalized energy over the whole frequency band.
To reduce the inclusion of noise, we consider the area below the leg contour curve and above the frequency line of 20.05kHz in Figure~\ref{pleg contour}, since the massive amount of energy above the leg contour is mostly incurred by frequency leakage.
With the measured gait cycle time (in Section~\ref{subsec:gaitcycletime}), the  Doppler signal in this area will be cut into a set of small pieces, each of a two-gait sample length covering the peak stride and mid stride that alternately appear \cite{kalgaonkar2007acoustic}.
Each two-gait sample $h[n]$ can be represented as follows \cite{degottex2010glottal}:
\begin{equation}
h[n]=u[n] \otimes v[n], \label{eq5}
\end{equation}
where $n$ represents a specific point on the discrete signal, $u[n]$ represents the pure Doppler signal generated by human motion, and $v[n]$ represents the vocal tract information which models the source of most interference. 
We apply the Fast Fourier Transform (FFT) to a two-gait sample as follows:
\begin{equation}
H\left(e^{j \omega}\right)=U\left(e^{j \omega}\right) V\left(e^{j \omega}\right), \label{eq6}
\end{equation}
where $\omega$ represents the angular frequency of the signal,  $H\left(e^{j \omega}\right)$, $U\left(e^{j \omega}\right)$, and  $V\left(e^{j \omega}\right)$ represent the Fourier transformation of $h[n]$, $u[n]$, and $v[n]$, respectively.
Then, a power spectrum is calculated  by entry-wise multiplication of the FFT coefficients and conjugates. 
A log operator is applied to the power spectrum to build a log power spectrum, i.e., 
%\begin{small}
\begin{equation}
\log \left(\left|H\left(e^{j \omega}\right)\right|^{2}\right)=\log \left(\left|U\left(e^{j \omega}\right)\right|^{2}\right)+\log \left(\left|V\left(e^{j \omega}\right)\right|^{2}\right), \label{eq7}
\end{equation}
%\end{small}
which shows that the power spectrum of Doppler signal  and vocal tract information  are linearly added together in the log power spectrum.
We can separate the Doppler signal from the vocal tract information through Inverse FFT (IFFT):
\begin{equation}
c_{h}[n]=c_{u}[n]+c_{v}[n]. \label{eq8}
\end{equation}
As we only consider the real cepstrum, taking IFFT to log power spectrum is equivalent to performing FFT.
Hence, the cepstrum can be viewed as the frequency decomposition of the log power spectrum.
The left side is the low-frequency component, representing the frequency decomposition of the log power spectrum envelope, including the  biometric information.
The right side consists of the high-frequency components which represent the frequency decomposition of the log power spectrum details for the vocal tract information.
Therefore, in Equation~(\ref{eq8}), $n$ represents a specific point on the cepstrum, $c_{h}[n]$ represents the cepstrum of received signal, $c_{u}[n]$ is the left side of the cepstrum coming from the Doppler signal, and $c_{v}[n]$ is the right side of the cepstrum coming from vocal tract information.
We pick up the top 100 points on the cepstrum's left side to form a  cepstral coefficient vector, which can describe the energy change of Doppler shift in each two-gait sample.


The {\em second} feature is the spectrogram signature \cite{wang2016gait}.
For each two-gait sample, we apply the 128 points FFT where the length of the Hamming window is equal to that of the respective two-gait sample.
As a result, a 128 point FFT sequence will be built for each two-gait sample.
The amplitude of each point represents the energy level on a specific frequency.
We continue to  normalize the energy  of each FFT sequence into the same scale (i.e., between 0 and 1). 
After normalization, each sample is used to calculate the normalized energy by deriving the mean magnitudes of every 5 adjacent FFT points. 
This will result in a total of 124 energy points, among which we drop the last three  and select 60 points with the interval of one to serve as the spectrogram signature of each sample.

The {\em third} feature is the ratio curve between the leg and torso speed curves.
	The leg and torso speed curves can be respectively calculated by using the frequency contours of the leg and the torso. 
	Notably, the leg frequency, as discussed before, represents the Doppler shift frequency caused by leg movement, which quantifies the moving speed of the leg in each two-gait cycle.
	The torso frequency contour can be obtained from Equation~(\ref{leg contour}).
	That is, we set the threshold value of $P(f, t)$ as $50\%$ to derive the frequency~\cite{van2008feature}, allowing us to get torso frequency of each two-gait sample. 
	After getting the leg and torso frequency curves, we can apply  Equation~(\ref{velocity}) to get the leg speed curve and torso speed curve, respectively, which together can characterize the motion of limbs during walking.
	Instead of directly using the leg speed curve and torso speed curve as features, we calculate the ratio values between each speed value on the leg speed curve and that on the  torso speed curve, to get the ratio curve. 
	Since the torso and leg are likely  in the same direction even a person changes walking directions, such a ratio curve will be relatively robust to the direction change.
	We save the values on this ratio curve into a fixed length vector to serve as our third feature.



The gait-cycle time, which quantifies  the duration of a gait cycle, will be used as the {\em fourth} feature.







In the end, we obtain four categories of features from each two-gait cycle to represent an individual's unique gait pattern. 
We would like to note that the specific features of leg speed, step length, cadence, step width, angle, and gait phases are not directly extracted.
However, these factors have significant effects on the Doppler signal.
For example, the leg speed and step length directly affect the shape of the contours, and cadence impacts the length of contours.
The step width and step angle affect the energy distribution of the Doppler signal, thereby signifying the contour pattern.
In addition, the two-step period contains the periodic change in the gait phases.
Therefore, we can conclude that the extracted features contain leg speed, step length, cadence, step width, angle, and gait phases information.


\vspace{-0.5em}
\subsection{Recognition}
\label{sec:recognition}



Based on the features extracted from aforementioned steps, we are ready to use the detected gait patterns to serve the purpose of intrusion detection and individual recognition. 
Essentially, our system is based on gait recognition to distinguish between the gait patterns of  intruders and  family members. 

The received acoustic signal  will be processed by EchoSensor at each module with detailed steps as aforementioned to extract the features.
Here, the Principal Component Analysis (PCA) \cite{wold1987principal} is applied to help reduce the dimensionality of the features by extracting the principal components and reducing the dimension of features from 222 to $N$.
These features will be trained in the {\em Machine Learning Classifier module} using the Support Vector Machine (SVM)  to acquire a unique model for each family member, so that any two-gait instance can be classified into two classes of self-gait and non-self-gait. 
With reduced dimensions, SVM can act with shorter training time and higher accuracy, compared with using the original data.
We propose to use the LibSVM tool \cite{chang2011libsvm} with the Radial Basis Function (RBF) kernel in the training phase.
The optimal values of parameters $n$ and $g$ for the RBF kernel are selected through grid search, which  takes less than $10s$.
It is worth noting that, to maintain the robustness of EchoSensor, we can enable it to periodically collect the two-gait instances of each family member so as to capture a rich set of gait patterns.
Finally, the SVM calculates the fitness probability of a newly appeared two-gait instance belonging to the enrolled family members or not.





