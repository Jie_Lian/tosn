\section{PERFORMANCE EVALUATION}
\label{sec:performance}

In this section, we conduct experiments to evaluate the performance of EchoSensor in the real home environments.


\subsection{Experiments}
\label{subec:Experiments}


EchoSensor is implemented on a Huawei P20 smartphone with Android 8.1 and a Bluetooth speaker Bose soundlink Revolve.
The smartphone works as the tone generator and controls the Bose soundlink Revolve speaker via Bluetooth connection to emit the ultrasonic signals at 20kHz.
\footnote{The sound pressure at 1 m from the speakers will reduce to 36dB, which will not cause side effects such as headache, dizziness, and pressure or pain in the ears.}
The built-in microphone on smartphone is used as the recorder.
We set the sampling frequency as 44.1kHz according to the Nyquist sampling theorem.
The recorded Doppler data is sent to a laptop for further  processing, with Matlab employed as the signal processing and machine learning tools. 

\smallskip
\noindent{\bf Data Collection.} \ Our experiments were conducted in 10 real family houses with a total of 24 participants. The number of family members in each household ranges from 1 to 4.
There are 10 females and 14 males, with their ages ranging from 20 to 50. 
The experiments was approved by our University’s IRB. 
Initially, the background noise was recorded at each house with no person moving. 
Each participant walked in his house toward and away from the speaker (up to 7 meters away) at his natural pace, repeatedly for 40 times.
In each house, two strangers were introduced as the intruders, each walking 20 round trips. 
Notably, each participant walked in their natural manner, not necessarily walking along the straight line to the speaker.
The reflected Doppler signal was recorded by the smartphone and processed at the laptop, with 200 two-gait instances collected from each participant. 


\smallskip
\noindent{\bf Evaluation Metrics.} \ 
To measure the sensing capability of EchoSensor in detecting intruders and family members, we use two stringent and fine-grained metrics of gait instances identification accuracy: Intruder Gait Detection Rate (IDR) and True Family Member Gait Detection Rate (TFR).
Specifically, IDR represents the ratio of a true intruder's two-gait instances detected over his total ones.
TFR represents the ratio of all detected true family members' two-gait instances over all detected ones claimed to belong to family members.
Notably, in the real-world deployment, a threshold (e.g., $50\%$ or more) for the IDR can be set to safely trigger the intrusion alarm, as will be elaborated in Section~\ref{subsec:overall}. 








\subsection{Operational Distance}
\label{subec:distance}
%\vspace{-0.5em}



We first conduct experiments to identify the best operational distance of EchoSensor.
For our collected data, we roughly truncate the two-gait instances at seven distance ranges, i.e., $(0, 1m), $ $ (1m, 2m),$ $\cdots,$ $ (6m, 7m)$.
For each family member, we train seven SVM classifiers corresponding to each distance range.
Figures~\ref{Distance_1},~\ref{Distance_2},~\ref{Distance_3} show the IDR and TFR at different distance ranges in three households, where $2m$ represents the distance range of $(1m, 2m)$ and the same for others.
We can see that EchoSensor achieves the best gait instance detection accuracy at the distance range of 1 to 5 meters, with IDR and TFR both more than $92\%$.
When the distance is more than 5 meters, both IDR and TFR quickly drop, with only $50\%$ accuracy at 7 meters.
The reason is that the energy of the reflected Doppler signals is too weak to be detected if the participant is far away from the speaker.
Interestingly, we also observe that both IDR and TFR perform worse within 1 meter.
This is due to the fact that within a close distance, most signals are reflected from the feet movement, hindering the microphone from capturing sufficient gait patterns for identification.

In summary, we can conclude that the appropriate operational distance for EchoSensor ranges from 1 to 5 meters for the accurate gait recognition, which is used as the basis to perform the following set of experiments.
Figure~\ref{Distance} also affirms our conclusion, which exhibits the averaged IDR and TFR values for all 10 families.
We believe such an operational distance is sufficient for EchoSensor to work in the smart home environments, where the audio devices typically are placed in the center of a room.
Moreover, there may be multiple audio devices in each home, placed at different positions or rooms, which can all install EchoSensor and work together to cover the house's majority area.






\begin{figure*}
	\centering
	\begin{minipage}{0.30\textwidth}
		\centering
		\includegraphics[width=1.9in]{Fig/Home1.pdf}
		%	\vspace{-0.6em}
		\caption{IDR and TFR at different distances in home 1.}
		\label{Distance_1}
		
	\end{minipage}
	\hspace{1em}
	\begin{minipage}{0.30\textwidth}
		\centering
		\includegraphics[width=1.9in]{Fig/Home2.pdf}
		%	\vspace{-0.6em}
		\caption{IDR and TFR at different distances in home 2.}
		\label{Distance_2}
		
	\end{minipage}
	\hspace{1em}
	\begin{minipage}{0.30\textwidth}
		\centering
		\includegraphics[width=1.9in]{Fig/Home3.pdf}
		%	\vspace{-0.6em}
		\caption{IDR and TFR at different distances in home 3.}
		\label{Distance_3}
		
	\end{minipage}
	\vspace{-1.0em}
\end{figure*}


	\begin{figure*}
			\begin{minipage}{0.24\textwidth}
	\centering
	\includegraphics[width=1.8in]{Fig/Distance.pdf}
 	\vspace{-1.5em}
	\caption{Averaged IDR and TFR at different distances.} % for all the 10 families
	\label{Distance}
%	\vspace{-1.5em}
	\end{minipage}
\hspace{0.8em}
	\begin{minipage}{0.35\textwidth}
			\centering
		\includegraphics[width=2.3in]{Fig/Different_home.pdf}
%		\vspace{-1.8em}
		\caption{Averaged IDR and TFR in all 10 families.}
		\label{Families}
%		\vspace{-1.5em}
			\end{minipage}
		\hspace{0.5em}
			\begin{minipage}{0.32\textwidth}
					\centering
				%	\vspace{1em}
				\includegraphics[width=1.9in]{Fig/Different_threshold.pdf}
				\caption{Alarm accuracy under different numbers of samples and thresholds.}
	%			\vspace{-1.5em}
				\label{threshold}
				\end{minipage}
				%	\vspace{-1.0em}
\end{figure*}



\subsection{Overall Performance}
\label{subsec:overall}
%\vspace{-0.5em}
Having identified the appropriate operational distances, we next present the performance of EchoSensor in all the 10 households.
In each household, we use each family member's data between 1 to 5 meters to train his SVM classifier.
All family members' data will be considered as positive samples.
Meanwhile, we take another 3 participants who are neither family members nor intruders as the negative samples in each SVM classifier's training.
For recognition, we set a detection threshold as 0.5, which means that EchoSensor classifies the gait sample as a family member when the corresponding fitness probability is higher than 0.5; as an intruder, otherwise.
The SVM classifier is trained with 5-fold cross-validation, where one-fifth of the family members' two-gait instances are used for testing and the remaining ones are used for training. 

Figure.~\ref{Families} illustrates the IDR and TFR of EchoSensor in each home.
The maximum IDR and TFR values are achieved at home 5, with $93.5\%$ and $92.9\%$, respectively.
The minimum IDR and TFR are  $91.5\%$ and $90.7\%$, respectively, at home 8.
The averaged IDR and TFR values across all 10 families are $92.7\%$ and $91.9\%$, respectively, demonstrating that EchoSensor can detect the subtle gait patterns of both intruders and family members with high accuracy.

{\em Notably, the IDR and TFR  here represent the ratios of the detected two-gait instances to validate the robustness of our system. 
In the real-world deployment, a lower threshold for IDR (say $50\%$) is sufficient to trigger the intrusion detection alarm safely.  }
To claim an intruder, we can set an alarm threshold, which specifies the  percentage of two-gait samples detected that is  unrecognized. 
We then define an alarm accuracy  as the possibility of  one individual truly identified as an intruder given a set of his two gait samples.
Figure~\ref{threshold} presents the alarm  accuracy with various number of detected two-gait samples when setting the alarm threshold to be $50\%$ and $70\%$.
We observe that alarm accuracy reaches
 $98\%$ and $94\%$ with the thresholds of $50\%$ and $70\%$, respectively, with 5 two-gait samples.
This result demonstrates that 5 two-gait samples are sufficient to trigger an alarm.
When using 15 two-gait samples, both values of alarm accuracy increase to $100\%$, given alarm threshold values of $50\%$ and $70\%$, respectively.
Despite a fully reliable detection, it may not be necessary to use 15 two-gait samples for the practical concern of space limitation. We prefer to use 5 two-gait samples to set up our intrusion detection alarm system in practice. 


\subsection{Impact of Extracted Features}
%\vspace{-0.5em}

To signify the effectiveness  of   four features,
we conduct another set of experiments, each including  three features, with
Figure~\ref{different_features} showing the respective performance.
 When all four features are included, EchoSensor achieves the highest values of IDR and TFR with $93\%$ and $91.9\%$, respectively. When one of the cepstral vector, spectrogram signature, leg contour,  and torso speed and gait cycle time pair is excluded, the resulted IDRs are $85.5\%$, $86.5\%$, $88\%$, and $80.5\%$, respectively, and the TFRs decrease to $84.7\%$, $85.7\%$, $87.3\%$, and $79.5\%$, respectively.
We observe the leg contour has relatively slight impact to the detection accuracy. However, it is  importance to help us determine gait-cycle time as discussed in Section~\ref{subsec:gaitcycletime}.

\begin{figure}
	\centering
	\begin{minipage}{0.4\textwidth}
		\centering
		\includegraphics[width=2.5in, height=1.6in]{Fig/different_features.pdf}
		\vspace{-2.5em}
		\caption{Different features.}
		\label{different_features}
%	\vspace{-2.0em}
	\end{minipage}
\hspace{2.0em}
	\begin{minipage}{0.40\textwidth}
		\centering
		\includegraphics[width=2.3in, height=1.4in]{Fig/different_actions.pdf}
		\vspace{-1.0em}
		\caption{Different actions.}
		\label{Different action}
	\end{minipage}
\end{figure}









\subsection{Impact of Person Actions}
\label{subsec:actions}

We then consider some unusual behaviors of an intruder when he invades into a house.
Four different actions are considered: 1) walking with  normal pace; 2) keeping arms stable to avoid touching something accidentally; 3) carrying a book stolen from the house; 4) randomly walking in front of  Echosensor to look for objects in the victim’s home.
We conducted our experiments in three homes, where two intruders walked with four different actions.
Figure~\ref{Different action} shows the averaged IDRs for each action in each home.
It is seen that EchoSensor can still achieve more than $90\%$ of IDRs under two unusual actions (no arm swing and carrying a book) in all three homes.
This demonstrates that EchoSensor is robust to different actions of an intruder: the gait pattern may be impacted by different actions, but it is still distinctive from family members and thus can be detected.
When the intruder is  walking in the house, the EchoSensor can still achieve more than $80\%$ of IDRs in all three homes, leading to a higher alarm accuracy as we have discussed in Section~\ref{subsec:overall}.



\subsection{Impact of Mimicry and Saturate Attack}
\label{subsec:Mimicry}


For mimicry attack, we consider the scenarios where an intruder may be familiar with the family members and try to mimic their walking patterns to obfuscate the system. 
Four participants were asked to try their best to walk in the same way, such as walking at the same speed and swinging their arms in the same amplitude.
We select three participants to be the family members and the remaining one to be the intruder.
Figure~\ref{Mimic} shows the results of IDRs and TFRs under four different combinations, treated as four families. 
We  obverse that the mimicry attack has negligible impact on the intrusion detection accuracy of  EchoSensor system.
This demonstrates that EchoSensor is able to capture rich fine-grained information in gait patterns and is robust to mimicry attacks.

For saturate attack, we also choose one participant as the intruder and the other three as  family members, with the total of four combinations, considering as four families.
In each family, the intruder carries a speaker playing  20khz ultrasonic sound to obfuscate the system.
Figure~\ref{Saturate} shows the results of IDRs and TFRs in four families under the saturate attack. 
It is observed that EchoSensor can still maintain the high IDRs and TFRs, demonstrating its robustness to saturate attack. 







\subsection{Impact of Device Placement}
As the  devices may be deployed at different places, such as on the floor, on a coffee table, on a chair, we now exam the impact of device placement, especially from the perspective of height, on the performance of EchoSensor.
Figure~\ref{Different_height} shows the results of IDRs and TFRs at four different heights (0m, 0.2m, 0.5m and 1m).
With a low height of 0m and 0.2m, the IDR and TFR can be higher than $90\%$.
When the height grows to 0.5m and 1m, the IDR drops to $88\%$ and $82\%$, respectively, and the TFR drops to $87\%$ and $80.8\%$, respectively.
Notably, even when the IDR and TFR at 1m reduce to $80\%$, EchoSensor can still perform well in triggering intrusion alarm as we have discussed in the end of Section~\ref{subsec:overall}, by setting the threshold of intrusion detection alarm  as $70\%$ or $80\%$.



\begin{figure*}
	\centering
	\begin{minipage}{0.31\textwidth}
		\centering
		\includegraphics[width=2.0in, height=1.3in]{Fig/mimc.pdf}
		\vspace{-2em}
		\caption{Mimicry attack.}
	%	\vspace{-1em}
		\label{Mimic}
	\end{minipage}
	\hspace{0.3em}
	\begin{minipage}{0.31\textwidth}
		\centering
		\includegraphics[width=2.0in, height=1.3in]{Fig/saturate.pdf}
		\vspace{-2em}
		\caption{Saturate attack.}
%		\vspace{-1em}
		\label{Saturate}
	\end{minipage}
	\hspace{0.3em}
	\begin{minipage}{0.31\textwidth}
		\centering
		\includegraphics[width=2.0in, height=1.3in]{Fig/height.pdf}
		\vspace{-2em}
		\caption{Different heights.}
		\label{Different_height}
	\end{minipage}
\end{figure*}



\subsection{Impact of Different devices}
\label{subsec:devices}

We next evaluate the performance of EchoSensor implemented on different devices. 
Three different smartphones and Bluetooth speakers are used to implement our experiments, where we pair them as the Huawei P20 and Bose soundlink (device pair 1), Galaxy s9 plus and Amazon echo (device pair 2), iPhone 8 and Apple Homepod (device pair 3).
Four participants are involved in this experiment, with two treated as the family members and two as the intruders. For each participant, 200 two-gait samples are collected from each implementation.
As shown in Figure~\ref{Different devices}, EchoSensor achieves averaged IDRs of $92.5\%, 91.5\%, 92\% $ and TFRs of $91.9\%, 90.7\%, 90.4\%$, respectively, for implementations on three devices.
Clearly, despite slight differences in the IDRs, EchoSensor exhibits good performance regardless of the types of speakers.
 

\begin{figure*}
	\centering
	\begin{minipage}{0.30\textwidth}
		\centering
		\includegraphics[width=2in, height=1.2in]{Fig/devices.pdf}
		\vspace{-2em}
		\caption{Different devices.}
		\label{Different devices}
	\end{minipage}
	\hspace{1em}
	\begin{minipage}{0.30\textwidth}
		\centering
		\includegraphics[width=2in, height=1.2in]{Fig/Different_angle.pdf}
		\vspace{-2em}
		\caption{Different angles.}	
		\label{Different_angle}
	\end{minipage}
	\hspace{1em}
	\begin{minipage}{0.30\textwidth}
		\centering
		\includegraphics[width=2in, height=1.2in]{Fig/speed.pdf}
		\vspace{-2em}
		\caption{Different speeds.}
		\label{Different_speeds}
	\end{minipage}
\end{figure*}

\subsection{Impact of Walking Direction and Speed}
\label{subsec:direction and speed}





We  exam the performance of 
EchoSensor when the walking direction and the orientation of microphone have a certain angle. 
Figure~\ref{Different_angle} shows the results of IDRs and TFRs when such an angle varies from 0, 30, 60, to 90 degrees. 
Both IDR and TFR degrade with the growth  of angles.
When the angle increases to  $60$ degrees, they drop to  $84\%$ and $79\%$, respectively.
When the angle increases to  $90$ degrees, they drop to  $71\%$ and $69\%$, respectively.
The reason is that a larger angle will reduce the Doppler reflection, which leads to fewer Doppler signals captured by the microphone and thus lower detection accuracy.
However, the leg and arm movement still could be extracted to a certain extent, making the intruder's patterns distinctive from these of the family members.



We further evaluate EchoSensor when intruders walk at different speeds. 
In Figure~\ref{Different_speeds}, compared with normal speed,
both IDR and TFR increase (from $91.5\%$ to $94\%$, and from $90.7\%$ to $93\%$) when the intruders walk slowly.
The reason is that high-quality spectrogram can be obtained from slow walking, which allows EchoSensor to  extract more clear patterns. 
In contrast, when walking fast, the performance of IDR and TFR drop to $82\%$ and $82.1\%$, respectively.












\subsection{Impact of Noises}
\label{subsec:noises} 

We next  evaluate the robustness of EchoSensor when there exists a certain level of noise in smart home.
Theoretically, the low-frequency noises at home, such as music playing and people talking, can be simply removed by low-pass filtering.
However, due to the frequency leakage \cite{li2022experience}, these audible noises may impact the performance of EchoSensor. 
Hence, we conducted the following experiment:
the two-gait samples of family members were recorded in a silent environment, while an intruder's two-gait samples were recorded in three scenarios: silent, music playing, and people talking with loud footstep noise.
In noisy environments, both the IDRs and TFRs drop below $85\%$, as shown in Figure~\ref{Different_noises}, because the frequency leakage of such sounds significantly blurs the spectrogram. 
However, they  remain higher than $90\%$ when we choose 5 two-gait samples as a group, which demonstrates our system is reliable in a noisy environment.



\subsection{Impact at Different Time}
	We then evaluate the time consistency of gait pattern regarding our EchoSensor's performance. 
	We collect 100 gait samples from one user (family member) in February 2022 to build the EchoSensor classifier.
	His gait samples are then collected in April and July for testing, while one user from dataset described in Section \ref{subec:Experiments}  is considered as the intruder. 
Due to changes in the weather, the clothes of that user change at different times.
Figure \ref{Different_time} shows the performance at each month.
In each month, the IDR is 92\%, 90\%, and 92\%, and the TFR is 91.33\%, 88.9\%, and 91.13\%.
The stable result shows the gait pattern is resilient to time and clothing changes.

\begin{figure}
	\centering
	\begin{minipage}{0.30\textwidth}
		\centering
		\includegraphics[width=2in, height=1.2in]{Fig/_noises.pdf}
		\caption{Different noise.}
		
		\label{Different_noises}
	\end{minipage}
	\hspace{1em}
	\begin{minipage}{0.30\textwidth}
		\centering
		\includegraphics[width=2in, height=1.2in]{Fig/_time.pdf}
		\caption{Different time.}
		%	\vspace{-1.5em}
		\label{Different_time}
	\end{minipage}	
	\hspace{1em}
	\begin{minipage}{0.30\textwidth}
		\centering
		\includegraphics[width=2in, height=1.2in]{Fig/Different_apporach.pdf}
		\caption{ EchoSensor v.s Baselines.}
		%	\vspace{-2em}
		\label{Different_apporaches}
	\end{minipage}

\end{figure}

\subsection{Comparison with Other Approaches}
%\vspace{-0.5em}
The previous gait identification system was not aimed at intrusion detection, and their system working flow differs from ours.		
Specifically, the existing acoustic-based gait detection solution  \cite{kalgaonkar2007acoustic,zhang2007acoustic, wang2018gait} did not extract the fine-grained features for identification. 
Instead, they relied on professional devices and neural network models to compute the features by using the whole spectrogram as input.
\cite{xu2019acousticid} implemented with the COTS device, however, they do not consider the reflection in the different environments and do not contain the interference cancellation part.
We call the \cite{kalgaonkar2007acoustic,zhang2007acoustic, wang2018gait} as baseline 1 while \cite{xu2019acousticid} as baseline 2. 
We implement their approaches on the same devices as that of   EchoSensor without using professional devices. 
We used their original setting and trained the gait identification system to compare with EchoSensor.
From Figure~\ref{Different_apporaches}, we can observe that EchoSensor can achieve the averaged IDR and TFR of $92.7\%$ and $91.9\%$, while the baseline 1 and baseline 2 are only of f $62.2\%$ and $60.5\%$, and of $78\%$ and $82\%$, respectively. 
The major performance dropping for the two baselines is due to the missing of   interference Cancellation process or the fine-grained feature extraction process. 	
This experiment demonstrates the importance and necessity of fine-grained signal processing and feature extraction developed in EchoSensor for accurate intrusion detection.








